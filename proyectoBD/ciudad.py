import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class ciudad:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Ciudad")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_ciudad()
        self.config_buttons_ciudad()

    def config_treeview_ciudad(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id_ciu")
        self.treeview.heading("#1", text = "nom_ciudad")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_ciudad()
        self.root.after(0, self.llenar_treeview_ciudad)

    def config_buttons_ciudad(self):
        tk.Button(self.root, text="Insertar ciudad",
            command = self.__insertar_ciudad).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar ciudad",
            command = self.__modificar_ciudad).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar ciudad",
            command = self.__eliminar_ciudad).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_ciudad(self):
        sql = """select * from ciudad;"""
        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0],tags = "rojo")
            self.data = data


    def __insertar_ciudad(self):
        insertar_ciudad(self.proyecto, self)
    
    def __modificar_ciudad(self):
        if(self.treeview.focus() != ""):
            sql = "select * from ciudad where id_ciu = %(id_ciu)s"
            row_data = self.proyecto.run_select_filter(sql, {"id_ciu": self.treeview.focus()})[0]
            modificar_ciudad(self.proyecto, self, row_data)
           
    def __eliminar_ciudad(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Está seguro que quiere eliminarlo?", title = "Precaución") == True:
                sql = "delete from ciudad where id_ciu = %(id_ciu)s"
                self.proyecto.run_sql(sql, {"id_ciu": self.treeview.focus()})
                self.llenar_treeview_ciudad()


class insertar_ciudad:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('200x120')
        self.insert_datos.title("Insertar ciudad")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "nombre: ").place(x = 10, y = 10, width = 80, height = 20)

    def config_entry(self):
        self.entry_nom_ciu = tk.Entry(self.insert_datos)
        self.entry_nom_ciu.place(x = 110, y = 10, width = 80, height = 20)


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)


    def __insertar(self):
        sql = """insert ciudad (nom_ciu) 
        values (%(nom_ciu)s)"""
        self.proyecto.run_sql(sql, {
            "nom_ciu": self.entry_nom_ciu.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ciudad()
    

class modificar_ciudad:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('250x150')
        self.insert_datos.title("Modificar ciudad")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "nombre ciudad: ").place(x = 10, y = 10, width = 110, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nom_ciu = tk.Entry(self.insert_datos)
        self.entry_nom_ciu.place(x = 120, y = 10, width = 110, height = 20)

       
        self.entry_nom_ciu.insert(0, self.row_data[1])
       


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =70, width = 80, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update ciudad set nom_ciu = %(nom_ciu)s 
            where id_ciu = %(id_ciu)s"""
        self.proyecto.run_sql(sql, {"id_ciu": int(self.row_data[0]),
            "nom_ciu": self.entry_nom_ciu.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_ciudad()
