from tkinter import *
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg)

class grafico_barras:
    def __init__(self, root, db):
        self.root = root
        self.db = db
        self.graph = Toplevel()
        fig, ax = plt.subplots()
        x, y = self.__get_data()
        ax.set_title("Estudiantes por carrera")
        ax.set_ylabel("Cantidad de estudiantes")
        ax.set_xlabel("carrera")
        ax.bar(x, y, color = "green")
        canvas = FigureCanvasTkAgg(fig, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()

    def __get_data(self):
        sql = """select carrera.nom_carrer, count(estudiante.nombre) from carrera
                 join estudiante on estudiante.carrera_id_carrer = carrera.id_carrer
                 group by carrera.nom_carrer"""
        data = self.db.run_select(sql)
        x = [i[0] for i in data]
        y = [i[1] for i in data]
        return x, y
