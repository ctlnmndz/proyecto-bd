import tkinter as tk
from tkinter import Menu
from tkinter import LabelFrame, Label, Frame
from tkinter import Button
from PIL import Image, ImageTk

#
from database import Database
from grafico import grafico_barras
from estudiante import estudiante
from carrera import carrera
from ciudad import ciudad
from beca import beca
from beneficio import beneficio
from tipo_recurso import tipo_recurso
from recurso import recurso
from registro_recurso import registro_recurso
from registro_beca import registro_beca
from registro_beneficio import registro_beneficio
from scatter import scatter
from funcion1 import funcion1
from trigger1 import trigger1
from vista import mostrar_vista


class App:
    def __init__(self, db):
        self.db = db

        # Main window
        self.root = tk.Tk()

        # Algunas especificaciones de tamaño y título de la ventana
        self.root.geometry("900x500")
        self.root.title("")

        #
        self.__crea_menubar()
        self.__crea_botones_principales()
        self.__agrega_imagen_principal()

        # Empieza a correr la interfaz.
        self.root.mainloop()

    # menubar
    def __crea_menubar(self):
        menubar = Menu(self.root)
        self.root.config(menu=menubar)

        #
        file_menu = Menu(menubar, tearoff=False)
        file_menu.add_command (label='Salir',
                                command=self.root.destroy)

        grafico_menu = Menu(menubar, tearoff=False)
        grafico_menu.add_command(label = "Barra",
                                 command=self.__mostrar_grafica)
        grafico_menu.add_command(label = "Scatter",
                                 command = self.__scatter)

        funcion = Menu(menubar, tearoff=0)
        funcion.add_command(label = "Estudiantes por ciudad",
                                 command = self.funcion1)

        vista = Menu(menubar, tearoff=False)
        vista.add_command(label = "Estudiantes Carrera",
                                 command = self.__view_estudiantes)

        trigger = Menu(menubar, tearoff=0)
        trigger.add_command(label = "Estudiante por carrera",
                                 command = self.trigger1)

        #
        menubar.add_cascade(label = "Archivo", menu=file_menu)
        menubar.add_cascade(label = "Graficos", menu=grafico_menu)
        menubar.add_cascade(label = "Funciones", menu=funcion)
        menubar.add_cascade(label = "Vistas", menu=vista)
        menubar.add_cascade(label = "Triggers", menu=trigger)

    # botones principales.
    def __crea_botones_principales(self):
        padx = 7
        pady = 7

        #
        frame = LabelFrame(self.root, text="", relief=tk.GROOVE)
        frame.place(x=10, y=10, width=200, relheight=0.95)

        #
        b1 = Button(frame, text="Estudiantes", width=20)
        b1.grid(row=0, column=0, padx=padx, pady=pady)
        b1.bind('<Button-1>', self.__mostrar_estudiantes)

        #
        b2 = Button(frame, text="Carrera", width=20)
        b2.grid(row=1, column=0, padx=padx, pady=pady)
        b2.bind('<Button-1>', self.__mostrar_carrera)

        #
        b3 = Button(frame, text="Ciudad", width=20)
        b3.grid(row=2, column=0, padx=padx, pady=pady)
        b3.bind('<Button-1>', self.__mostrar_ciudad)

        #
        b4 = Button(frame, text="Beca", width=20)
        b4.grid(row=3, column=0, padx=padx, pady=pady)
        b4.bind('<Button-1>', self.__mostrar_beca)

        #
        b5 = Button(frame, text="Beneficio", width=20)
        b5.grid(row=4, column=0, padx=padx, pady=pady)
        b5.bind('<Button-1>', self.__mostrar_beneficio)

        #
        b6 = Button(frame, text="Tipo recurso", width=20)
        b6.grid(row=5, column=0, padx=padx, pady=pady)
        b6.bind('<Button-1>', self.__mostrar_tipo_recurso)

        #
        b7 = Button(frame, text="Recurso", width=20)
        b7.grid(row=6, column=0, padx=padx, pady=pady)
        b7.bind('<Button-1>', self.__mostrar_recurso)

        #
        b8 = Button(frame, text="Registro recurso", width=20)
        b8.grid(row=7, column=0, padx=padx, pady=pady)
        b8.bind('<Button-1>', self.__mostrar_registro_recurso)

        #
        b9 = Button(frame, text="Registro beca", width=20)
        b9.grid(row=8, column=0, padx=padx, pady=pady)
        b9.bind('<Button-1>', self.__mostrar_registro_beca)

        #
        b10 = Button(frame, text="Registro beneficio", width=20)
        b10.grid(row=9, column=0, padx=padx, pady=pady)
        b10.bind('<Button-1>', self.__mostrar_registro_beneficio)


    # imagen principal.
    def __agrega_imagen_principal(self):
        #
        frame = LabelFrame(self.root, text="", relief=tk.FLAT)
        frame.place(x=215, y=10, relwidth=0.68, relheight=0.95)

        image = Image.open("images.png")
        photo = ImageTk.PhotoImage(image.resize((550, 400), Image.ANTIALIAS))
        label = Label(frame, image=photo)
        label.image = photo
        label.pack()


    # muestra ventana equipos.
    def __mostrar_estudiantes(self, button):
        estudiante(self.root, self.db)

    # muestra ventana jugadores.
    def __mostrar_carrera(self, button):
        carrera(self.root, self.db)

    def __mostrar_ciudad(self, button):
        ciudad(self.root, self.db)

    def __mostrar_beca(self, button):
        beca(self.root, self.db)

    def __mostrar_beneficio(self, button):
        beneficio(self.root, self.db)

    def __mostrar_tipo_recurso(self, button):
        tipo_recurso(self.root, self.db)

    def __mostrar_recurso(self, button):
        recurso(self.root, self.db)

    def __mostrar_registro_recurso(self, button):
        registro_recurso(self.root, self.db)

    def __mostrar_registro_beca(self, button):
        registro_beca(self.root, self.db)

    def __mostrar_registro_beneficio(self, button):
        registro_beneficio(self.root, self.db)

    def __mostrar_grafica(self):
        grafico_barras(self.root, self.db)

    def __scatter(self):
        scatter(self.root, self.db)

    def funcion1(self):
        funcion1(self.root, self.db)

    def trigger1(self):
        trigger1(self.root, self.db)

    def __view_estudiantes(self):
        mostrar_vista(self.root, self.db)


"""
    def __graficos(self, button):
        graficos(self.root, self.db)"""

def main():
    # Conecta a la base de datos
    db = Database()

    # La app xD
    App(db)

if __name__ == "__main__":
    main()
