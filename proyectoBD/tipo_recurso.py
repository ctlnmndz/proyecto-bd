import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class tipo_recurso:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Tipo Recurso")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_tipo_recurso()
        self.config_buttons_tipo_recurso()

    def config_treeview_tipo_recurso(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "ID tipo recurso")
        self.treeview.heading("#1", text = "Nombre tipo recurso")
        self.treeview.heading("#2", text = "Cantidad recurso")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 300, stretch = False)
        self.treeview.column("#2", minwidth = 300, width = 500, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_tipo_recurso()
        self.root.after(0, self.llenar_treeview_tipo_recurso)

    def config_buttons_tipo_recurso(self):
        tk.Button(self.root, text="Insertar tipo recurso",
            command = self.__insertar_tipo_recurso).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar tipo recurso",
            command = self.__modificar_tipo_recurso).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar tipo recurso",
            command = self.__eliminar_tipo_recurso).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_tipo_recurso(self):
        sql = """select * from tipo_recurso;"""
        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2]), iid = i[0],tags = "rojo")
            self.data = data
        


    def __insertar_tipo_recurso(self):
        insertar_tipo_recurso(self.proyecto, self)
    
    def __modificar_tipo_recurso(self):
        if(self.treeview.focus() != ""):
            sql = "select * from tipo_recurso where id_tipo = %(id_tipo)s"
            row_data = self.proyecto.run_select_filter(sql, {"id_tipo": self.treeview.focus()})[0]
            modificar_tipo_recurso(self.proyecto, self, row_data)
           
    def __eliminar_tipo_recurso(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Está seguro que quiere eliminarlo?", title = "Precaución") == True:
                sql = "delete from tipo_recurso where id_tipo = %(id_tipo)s"
                self.proyecto.run_sql(sql, {"id_tipo": self.treeview.focus()})
                self.llenar_treeview_tipo_recurso()


class insertar_tipo_recurso:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('350x150')
        self.insert_datos.title("Insertar tipo recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "nombre tipo recurso: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "cantidad tipo recurso: ").place(x = 10, y = 30, width = 150, height = 20)

    def config_entry(self):
        self.entry_nom_tipo = tk.Entry(self.insert_datos)
        self.entry_nom_tipo.place(x = 160, y = 10, width = 150, height = 20)
        self.entry_cant_rec = tk.Entry(self.insert_datos)
        self.entry_cant_rec.place(x = 160, y = 30, width = 150, height = 20)


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)


    def __insertar(self):
        sql = """insert tipo_recurso (nom_tipo, cant_rec) 
        values (%(nom_tipo)s, %(cant_rec)s)"""
        self.proyecto.run_sql(sql, {
            "nom_tipo": self.entry_nom_tipo.get(),
            "cant_rec": self.entry_cant_rec.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_tipo_recurso()
    

class modificar_tipo_recurso:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('350x150')
        self.insert_datos.title("Modificar tipo recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "nombre tipo recurso: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "cantidad tipo recurso: ").place(x = 10, y = 30, width = 150, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nom_tipo = tk.Entry(self.insert_datos)
        self.entry_nom_tipo.place(x = 160, y = 10, width = 150, height = 20)
        self.entry_cant_rec = tk.Entry(self.insert_datos)
        self.entry_cant_rec.place(x = 160, y = 30, width = 150, height = 20)

       
        self.entry_nom_tipo.insert(0, self.row_data[1])
        self.entry_cant_rec.insert(0, self.row_data[2])
       


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =70, width = 80, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update tipo_recurso set nom_tipo = %(nom_tipo)s, cant_rec = %(cant_rec)s
            where id_tipo = %(id_tipo)s"""
        self.proyecto.run_sql(sql, {"id_tipo": int(self.row_data[0]),
            "nom_tipo": self.entry_nom_tipo.get(),
            "cant_rec": self.entry_cant_rec.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_tipo_recurso()
