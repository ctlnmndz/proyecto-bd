import tkinter as tk
from tkinter import ttk

from recurso import recurso
from estudiante import estudiante
from tkinter import messagebox

class registro_recurso:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Registro recursos")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_registro()
        self.config_buttons_registro()

    def config_treeview_registro(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Recurso")
        self.treeview.heading("#1", text = "matricula estudiante")
        self.treeview.heading("#2", text = "fecha entrega")
        self.treeview.heading("#3", text = "fecha recibido")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 260, width = 260, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 160, stretch = False)
        self.treeview.column("#3", minwidth = 100, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_registro()
        self.root.after(0, self.llenar_treeview_registro)

    def config_buttons_registro(self):
        tk.Button(self.root, text="Insertar registro",
            command = self.__insertar_fecha).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar registro",
            command = self.__modificar_fecha).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar registro",
            command = self.__eliminar_fecha).place(x = 400, y = 350, width = 200, height = 50)


    def llenar_treeview_registro(self):
        sql = """select recurso.id_rec, estudiante.matricula, fec_rec, fec_lim
        from regis_recurso join recurso join estudiante on recurso.id_rec = regis_recurso.recurso_id_rec
        and estudiante.matricula = regis_recurso.estudiante_matricula;"""

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data

    def __insertar_fecha(self):
        insertar_fecha(self.proyecto, self)

    def __modificar_fecha(self):
        if(self.treeview.focus() != ""):
            sql = """select recurso.id_rec, estudiante.matricula, fec_rec, fec_lim from regis_recurso
                join recurso join estudiante on recurso.id_rec = regis_recurso.recurso_id_rec and
                estudiante.matricula = regis_recurso.estudiante_matricula where recurso.id_rec = %(recurso.id_rec)s"""
            row_data = self.proyecto.run_select_filter(sql, {"recurso.id_rec": self.treeview.focus()})[0]
            modificar_fecha(self.proyecto, self, row_data)


    def __eliminar_fecha(self):
            if(self.treeview.focus() != ""):
                if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                    sql = "delete from regis_recurso where recurso_id_rec = %(recurso_id_rec)s"
                    self.proyecto.run_sql(sql, {"recurso_id_rec": self.treeview.focus()})
                    self.llenar_treeview_registro()


class insertar_fecha:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Fecha recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "recurso: ").place(x = 10, y = 10, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "matricula: ").place(x = 10, y = 40, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "Fecha entrega: ").place(x = 10, y = 80, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "Fecha recibo: ").place(x = 10, y = 110, width = 100, height = 20)

    def config_entry(self):
        self.combo_recurso = ttk.Combobox(self.insert_datos)
        self.combo_recurso.place(x = 110, y = 10, width = 180, height= 20)
        self.combo_recurso["values"], self.ids = self.fill_combo_recurso()
        self.combo_matricula = ttk.Combobox(self.insert_datos)
        self.combo_matricula.place(x = 110, y = 40, width = 180, height= 20)
        self.combo_matricula["values"], self.idsc = self.fill_combo_matricula()

        self.entry_fec_rec = tk.Entry(self.insert_datos)
        self.entry_fec_rec.place(x = 110, y = 80, width = 180, height = 20)
        self.entry_fec_lim = tk.Entry(self.insert_datos)
        self.entry_fec_lim.place(x = 110, y = 110, width = 180, height = 20)

    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =150, width = 200, height = 20)

    def fill_combo_recurso(self):
        sql = "select id_rec, nom_rec from recurso"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_combo_matricula(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self):
        sql = """insert into regis_recurso (recurso_id_rec, estudiante_matricula, fec_rec, fec_lim)
            values (%(recurso_id_rec)s, %(estudiante_matricula)s, %(fec_rec)s, %(fec_lim)s)"""
        self.proyecto.run_sql(sql, {"recurso_id_rec": self.ids[self.combo_recurso.current()],
            "estudiante_matricula": self.idsc[self.combo_matricula.current()],
            "fec_rec": self.entry_fec_rec.get(),
            "fec_lim": self.entry_fec_lim.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro()

class modificar_fecha:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Fecha recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "recurso: ").place(x = 10, y = 10, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "matricula: ").place(x = 10, y = 40, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "Fecha entrega: ").place(x = 10, y = 80, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "Fecha recibo: ").place(x = 10, y = 110, width = 100, height = 20)

    def config_entry(self):
        self.combo_recurso = ttk.Combobox(self.insert_datos)
        self.combo_recurso.place(x = 110, y = 10, width = 180, height= 20)
        self.combo_recurso["values"], self.ids = self.fill_combo_recurso()
        self.combo_matricula = ttk.Combobox(self.insert_datos)
        self.combo_matricula.place(x = 110, y = 40, width = 180, height= 20)
        self.combo_matricula["values"], self.idsm = self.fill_combo_matricula()

        self.entry_fec_rec = tk.Entry(self.insert_datos)
        self.entry_fec_rec.place(x = 110, y = 80, width = 180, height = 20)
        self.entry_fec_lim = tk.Entry(self.insert_datos)
        self.entry_fec_lim.place(x = 110, y = 110, width = 180, height = 20)

        self.combo_recurso.insert(0, self.row_data[0])
        self.combo_matricula.insert(0, self.row_data[1])
        self.entry_fec_rec.insert(0, self.row_data[2])
        self.entry_fec_lim.insert(0, self.row_data[3])

    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
        command = self.__modificar).place(x=0, y =150, width = 200, height = 20)

    def fill_combo_recurso(self):
        sql = "select id_rec, nom_rec from recurso"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_combo_matricula(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __modificar(self):
        sql = """update regis_recurso set recurso_id_rec = %(recurso_id_rec)s,
            estudiante_matricula = %(estudiante_matricula)s, fec_rec = %(fec_rec)s,
            fec_lim = %(fec_lim)s where recurso_id_rec = %(recurso_id_rec)s"""
        self.proyecto.run_sql(sql, {
            "recurso_id_rec": self.ids[self.combo_recurso.current()],
            "estudiante_matricula": self.idsm[self.combo_matricula.current()],
            "fec_rec": self.entry_fec_rec.get(),
            "fec_lim": self.entry_fec_lim.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro()
 
