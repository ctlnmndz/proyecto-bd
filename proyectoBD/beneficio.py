import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class beneficio:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Beneficio")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_beneficio()
        self.config_buttons_beneficio()

    def config_treeview_beneficio(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "nombre beneficio")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 300, width = 300, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_beneficio()
        self.root.after(0, self.llenar_treeview_beneficio)

    def config_buttons_beneficio(self):
        tk.Button(self.root, text="Insertar beneficio",
            command = self.__insertar_beneficio).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar beneficio",
            command = self.__modificar_beneficio).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar Beneficio",
            command = self.__eliminar_beneficio).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_beneficio(self):
        sql = """select * from beneficio;"""
        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0],tags = "rojo")
            self.data = data


    def __insertar_beneficio(self):
        insertar_beneficio(self.proyecto, self)

    def __modificar_beneficio(self):
        if(self.treeview.focus() != ""):
            sql = "select * from beneficio where id_bene = %(id_bene)s"
            row_data = self.proyecto.run_select_filter(sql, {"id_bene": self.treeview.focus()})[0]
            modificar_beneficio(self.proyecto, self, row_data)

    def __eliminar_beneficio(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Está seguro que quiere eliminarlo?", title = "Precaución") == True:
                sql = "delete from beneficio where id_bene = %(id_bene)s"
                self.proyecto.run_sql(sql, {"id_bene": self.treeview.focus()})
                self.llenar_treeview_beneficio()

class insertar_beneficio:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar beneficio")
        self.insert_datos.resizable(width=10, height=10)

    def config_label(self):
        tk.Label(self.insert_datos, text = "id beneficio: ").place(x = 10, y = 10, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "nombre: ").place(x = 10, y = 30, width = 100, height = 20)

    def config_entry(self):
        self.entry_id_bene = tk.Entry(self.insert_datos)
        self.entry_id_bene.place(x = 110, y = 10, width = 180, height = 20)
        self.entry_nom_bene = tk.Entry(self.insert_datos)
        self.entry_nom_bene.place(x = 110, y = 30, width = 180, height = 20)


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =100, width = 200, height = 20)

    def __insertar(self):
        sql = """insert beneficio (id_bene, nom_bene)
        values (%(id_bene)s, %(nom_bene)s)"""
        self.proyecto.run_sql(sql, {"id_bene": self.entry_id_bene.get(),
            "nom_bene": self.entry_nom_bene.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_beneficio()


class modificar_beneficio:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar beneficio")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "Nombre : ").place(x = 10, y = 10, width = 100, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nom_bene = tk.Entry(self.insert_datos)
        self.entry_nom_bene.place(x = 110, y = 10, width = 180, height = 20)
        

        self.entry_nom_bene.insert(0, self.row_data[1])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update beneficio set nom_bene = %(nom_bene)s
                 where id_bene = %(id_bene)s"""
        self.proyecto.run_sql(sql, {"id_bene": int(self.row_data[0]),
            "nom_bene": self.entry_nom_bene.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_beneficio()
