import tkinter as tk
from tkinter import ttk
from tkinter import Button


class trigger1:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []
        # Ventana emergente
        self.root = tk.Toplevel()
        # Ajustes de ventana
        self.root.geometry('500x300')
        self.root.title("Información estudiantes por carrera")
        self.root.resizable(width = 0, height = 0)
        # toplevel modal
        self.root.transient(root)


        # Funcionalidades
        self.__config_treeview_trigger()

        # Crear trigger
        self.generar_trigger()

        self.llenar_treeview_trigger()

    def generar_trigger(self):
        if(self.treeview.focus() != ""):
            sql = """create table etudiante_ciudad (id_estudiante int not null primary key auto_increment,
                    matricula varchar(50), nombre varchar(50), apellido varchar(50), carrera varchar(50));
                    delimiter //
                    create trigger trigger_estudiante_ciudad after
                    insert on estudiante for each row begin insert into estudiante_ciudad
                    (matricula, nombre, apellido, ciudad) values
                    (NEW.matricula, NEW.nombre, NEW.apellido, current_carrera); end;//
                    delimiter ;"""
            self.libreria.run_sql(sql, {"matricula": self.treeview.focus()})


    def __config_treeview_trigger(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#0", "#1", "#2", "#3"))
        self.treeview.heading("#0", text = "matricula")
        self.treeview.heading("#1", text = "Nombre")
        self.treeview.heading("#2", text = "Apellido")
        self.treeview.heading("#3", text = "carrera")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 135, width = 135, stretch = False)
        self.treeview.column("#2", minwidth = 135, width = 135, stretch = False)
        self.treeview.column("#3", minwidth = 135, width = 135, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 300, width = 500)
        self.llenar_treeview_trigger()
        self.root.after(0, self.llenar_treeview_trigger)

    def llenar_treeview_trigger(self):
        sql = """select matricula, nombre, apellido, carrera from estudiante_ciudad;"""

        # Guarda info obtenida tras la consulta
        data = self.proyecto.run_select(sql)

        # Evalúa si el contenido de la tabla en la app es distinto al de la db
        if(data != self.data):
            # Elimina todos los rows del treeview si hay diferencias
            self.treeview.delete(*self.treeview.get_children())

            for i in data:
                # Inserta valores en treeview
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]))

            self.data = data
