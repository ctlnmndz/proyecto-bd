import tkinter as tk
from tkinter import ttk

from carrera import carrera
from ciudad import ciudad
from tkinter import messagebox

class estudiante:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []
        
        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('1000x400')
        self.root.title("Estudiantes")
        self.root.resizable(width=0, height=0)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.__config_treeview_estudiante()
        self.__config_buttons_estudiante()

    def __config_treeview_estudiante(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3", "#4", "#5", "#6", "#7", "#8"))
        self.treeview.heading("#0", text = "matricula")
        self.treeview.heading("#1", text = "nombre")
        self.treeview.heading("#2", text = "apellido")
        self.treeview.heading("#3", text = "telefono")
        self.treeview.heading("#4", text = "rut")
        self.treeview.heading("#5", text = "carrera")
        self.treeview.heading("#6", text = "ciudad")
        self.treeview.heading("#7", text = "fecha ingreso")
        self.treeview.heading("#8", text = "correo")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#3", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#4", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#5", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#6", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#7", minwidth = 100, width = 150, stretch = False)
        self.treeview.column("#8", minwidth = 100, width = 150, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 1100)
        self.llenar_treeview_estudiante()
        self.root.after(0, self.llenar_treeview_estudiante)

    def __config_buttons_estudiante(self):
        tk.Button(self.root, text="Insertar estudiante", 
            command = self.__insertar_estudiante).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar estudiante", 
            command = self.__modificar_estudiante).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar estudiante", 
            command = self.__eliminar_estudiante).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_estudiante(self):
        sql = """select matricula, nombre, apellido, telefono, rut, fec_ingreso, correo, carrera.nom_carrer, ciudad.nom_ciu 
            from estudiante join carrera join ciudad on carrera.id_carrer = estudiante.carrera_id_carrer and
            ciudad.id_ciu = estudiante.ciudad_id_ciu;"""
       
        data = self.proyecto.run_select(sql)
        
        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0], 
                    values = (i[1], i[2], i[3], i[4], i[5], i[6], i[7], i[8]), iid = i[0])
            self.data = data
        

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2]), iid = i[0])
            self.data = data  

    def __insertar_estudiante(self):
        insertar_estudiante(self.proyecto, self)

    def __modificar_estudiante(self):
        if(self.treeview.focus() != ""):
            sql = "select * from estudiante where matricula = %(matricula)s"
            row_data = self.proyecto.run_select_filter(sql, {"matricula": self.treeview.focus()})[0]
            modificar_estudiante(self.proyecto, self, row_data)

    def __eliminar_estudiante(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                sql = "delete from estudiante where matricula = %(matricula)s"
                self.proyecto.run_sql(sql, {"matricula": self.treeview.focus()})
                self.llenar_treeview_estudiante()


class insertar_estudiante:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('400x350')
        self.insert_datos.title("Insertar estudiante")
        self.insert_datos.resizable(width=10, height=10)

    def config_label(self):
        tk.Label(self.insert_datos, text = "matricula: ").place(x = 10, y = 10, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "nombre: ").place(x = 10, y = 40, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "apellido: ").place(x = 10, y = 70, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "telefono: ").place(x = 10, y = 100, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "rut: ").place(x = 10, y = 130, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "carrera: ").place(x = 10, y = 160, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "ciudad: ").place(x = 10, y = 190, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "fecha ingreso: ").place(x = 10, y = 220, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "correo: ").place(x = 10, y = 250, width = 100, height = 20)
        
    def config_entry(self):
        self.entry_matricula = tk.Entry(self.insert_datos)
        self.entry_matricula.place(x = 110, y = 10, width = 200, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 40, width = 200, height = 20)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x = 110, y = 70, width = 200, height = 20)
        self.entry_telefono = tk.Entry(self.insert_datos)
        self.entry_telefono.place(x = 110, y = 100, width = 200, height = 20)
        self.entry_rut = tk.Entry(self.insert_datos)
        self.entry_rut.place(x = 110, y = 130, width = 200, height = 20)
        self.combo_carrera = ttk.Combobox(self.insert_datos)
        self.combo_carrera.place(x = 110, y = 160, width = 180, height= 20)
        self.combo_carrera["values"], self.ids = self.fill_combo_carrera()
        self.combo_ciudad = ttk.Combobox(self.insert_datos)
        self.combo_ciudad.place(x = 110, y = 190, width = 180, height= 20)
        self.combo_ciudad["values"], self.idsc = self.fill_combo_ciudad()
        self.entry_fec_ingreso = tk.Entry(self.insert_datos)
        self.entry_fec_ingreso.place(x = 110, y = 220, width = 200, height = 20)
        self.entry_correo = tk.Entry(self.insert_datos)
        self.entry_correo.place(x = 110, y = 250, width = 200, height = 20)

        #self.combo = ttk.Combobox(self.insert_datos)
        #self.combo.place(x = 110, y = 130, width = 200, height= 20)
        #self.combo["values"], self.ids = self.__fill_combo()


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.__insertar).place(x=75, y =290, width = 200, height = 20)


    def fill_combo_carrera(self):
        sql = "select id_carrer, nom_carrer from carrera"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
    
    def fill_combo_ciudad(self):
        sql = "select id_ciu, nom_ciu from ciudad"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert estudiante ( matricula, nombre, apellido, telefono, rut, carrera_id_carrer, ciudad_id_ciu, fec_ingreso, correo) 
            values (%(matricula)s, %(nombre)s, %(apellido)s, %(telefono)s, %(rut)s, %(carrera_id_carrer)s, %(ciudad_id_ciu)s, %(fec_ingreso)s, %(correo)s)"""
        self.proyecto.run_sql(sql, {"matricula": self.entry_matricula.get(),
            "nombre": self.entry_nombre.get(),
            "apellido": self.entry_apellido.get(),
            "telefono": self.entry_telefono.get(),
            "rut": self.entry_rut.get(),
            "carrera_id_carrer": self.ids[self.combo_carrera.current()],
            "ciudad_id_ciu": self.idsc[self.combo_ciudad.current()],
            "fec_ingreso": self.entry_fec_ingreso.get(),
            "correo": self.entry_correo.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_estudiante()

class modificar_estudiante:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('400x350')
        self.insert_datos.title("Modificar estudiante ")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "matricula: ").place(x = 10, y = 10, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "nombre: ").place(x = 10, y = 40, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "apellido: ").place(x = 10, y = 70, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "telefono: ").place(x = 10, y = 100, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "rut: ").place(x = 10, y = 130, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "carrera: ").place(x = 10, y = 160, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "ciudad: ").place(x = 10, y = 190, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "fecha ingreso: ").place(x = 10, y = 220, width = 100, height = 20)
        tk.Label(self.insert_datos, text = "correo: ").place(x = 10, y = 250, width = 100, height = 20)
        
    def config_entry(self):#Se configuran los inputs
        self.entry_matricula = tk.Entry(self.insert_datos)
        self.entry_matricula.place(x = 110, y = 10, width = 200, height = 20)
        self.entry_nombre = tk.Entry(self.insert_datos)
        self.entry_nombre.place(x = 110, y = 40, width = 200, height = 20)
        self.entry_apellido = tk.Entry(self.insert_datos)
        self.entry_apellido.place(x = 110, y = 70, width = 200, height = 20)
        self.entry_telefono = tk.Entry(self.insert_datos)
        self.entry_telefono.place(x = 110, y = 100, width = 200, height = 20)
        self.entry_rut = tk.Entry(self.insert_datos)
        self.entry_rut.place(x = 110, y = 130, width = 200, height = 20)
        self.combo_carrera = ttk.Combobox(self.insert_datos)
        self.combo_carrera.place(x = 110, y = 160, width = 180, height= 20)
        self.combo_carrera["values"], self.ids = self.fill_combo_carrera()
        self.combo_ciudad = ttk.Combobox(self.insert_datos)
        self.combo_ciudad.place(x = 110, y = 190, width = 180, height= 20)
        self.combo_ciudad["values"], self.idsc = self.fill_combo_ciudad()
        self.entry_fec_ingreso = tk.Entry(self.insert_datos)
        self.entry_fec_ingreso.place(x = 110, y = 220, width = 200, height = 20)
        self.entry_correo = tk.Entry(self.insert_datos)
        self.entry_correo.place(x = 110, y = 250, width = 200, height = 20)
        
        self.entry_matricula.insert(0, self.row_data[0])
        self.entry_nombre.insert(0, self.row_data[1])
        self.entry_apellido.insert(0, self.row_data[2])
        self.entry_telefono.insert(0, self.row_data[3])
        self.entry_rut.insert(0, self.row_data[4])
        self.combo_carrera.insert(0, self.row_data[5])
        self.combo_ciudad.insert(0, self.row_data[6])
        self.entry_fec_ingreso.insert(0, self.row_data[7])
        self.entry_correo.insert(0, self.row_data[8])

    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado. 
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.modificar).place(x=0, y =280, width = 200, height = 20)

    def fill_combo_carrera(self):
        sql = "select id_carrer, nom_carrer from carrera"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
    
    def fill_combo_ciudad(self):
        sql = "select id_ciu, nom_ciu from ciudad"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def modificar(self): #Insercion en la base de datos. 
        sql = """update estudiante set nombre = %(nombre)s, apellido = %(apellido)s, 
            telefono = %(telefono)s, rut = %(rut)s, carrera_id_carrer = %(carrera_id_carrer)s, ciudad_id_ciu = %(ciudad_id_ciu)s,
            fec_ingreso = %(fec_ingreso)s, correo = %(correo)s where matricula = %(matricula)s"""
        self.proyecto.run_sql(sql, {"matricula": str(self.row_data[0]),
            #"matricula": self.entry_matricula.get(),
            "nombre": self.entry_nombre.get(),
            "apellido": self.entry_apellido.get(),
            "telefono": self.entry_telefono.get(),
            "rut": self.entry_rut.get(),
            "carrera_id_carrer": self.ids[self.combo_carrera.current()],
            "ciudad_id_ciu": self.idsc[self.combo_ciudad.current()],
            "fec_ingreso": self.entry_fec_ingreso.get(),
            "correo": self.entry_correo.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_estudiante()

