import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class beca:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Becas")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_beca()
        self.config_buttons_beca()

    def config_treeview_beca(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1"))
        self.treeview.heading("#0", text = "id beca")
        self.treeview.heading("#1", text = "nombre beca")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 200, stretch = False)

        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_beca()
        self.root.after(0, self.llenar_treeview_beca)

    def config_buttons_beca(self):
        tk.Button(self.root, text="Insertar beca",
            command = self.__insertar_beca).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar beca",
            command = self.__modificar_beca).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar beca",
            command = self.__eliminar_beca).place(x = 400, y = 350, width = 200, height = 50)

    def llenar_treeview_beca(self):
        sql = "select * from beca"
        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1]), iid = i[0])
            self.data = data

    def __insertar_beca(self):
        insertar_beca(self.proyecto, self)

    def __modificar_beca(self):
        if(self.treeview.focus() != ""):
            sql = "select * from beca where id_beca = %(id_beca)s"
            row_data = self.proyecto.run_select_filter(sql, {"id_beca": self.treeview.focus()})[0]
            modificar_beca(self.proyecto, self, row_data)

    def __eliminar_beca(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Está seguro que quiere eliminarlo?", title = "Precaución") == True:
                sql = "delete from beca where id_beca = %(id_beca)s"
                self.proyecto.run_sql(sql, {"id_beca": self.treeview.focus()})
                self.llenar_treeview_beca()


class insertar_beca:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('300x250')
        self.insert_datos.title("Insertar Beca")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "nombre beca: ").place(x = 10, y = 10, width = 100, height = 20)

    def __config_entry(self):
        self.entry_nom_bec = tk.Entry(self.insert_datos)
        self.entry_nom_bec.place(x = 110, y = 10, width = 150, height = 20)
        #self.combo = ttk.Combobox(self.insert_datos)
        #self.combo.place(x = 110, y = 70, width = 80, height= 20)
        #self.combo["values"], self.id_beca = self.fill_combo()

    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=110, y =100, width = 150, height = 20)

    #def fill_combo(self):
        #sql = "select id_beca, nom_bec, beca_act, fec_bec from beca"
        #self.data = self.proyecto.run_select(sql)
        #return [i[1] for i in self.data], [i[0] for i in self.data]

    def __insertar(self): #Insercion en la base de datos.
        sql = """insert beca (nom_bec)
            values (%(nom_bec)s)"""
        self.proyecto.run_sql(sql, {
            "nom_bec": self.entry_nom_bec.get(),})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_beca()

class modificar_beca:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('250x150')
        self.insert_datos.title("Modificar beca")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self): #Labels
        tk.Label(self.insert_datos, text = "nombre_beca: ").place(x = 10, y = 10, width = 110, height = 20)

    def config_entry(self):#Se configuran los inputs
        self.entry_nom_bec = tk.Entry(self.insert_datos)
        self.entry_nom_bec.place(x = 120, y = 10, width = 110, height = 20)
        #self.combo = ttk.Combobox(self.insert_datos)
        #self.combo.place(x = 110, y = 70, width = 80, height= 20)
        #self.combo["values"], self.id_beca = self.fill_combo()

        self.entry_nom_bec.insert(0, self.row_data[1])
        #self.combo.insert(0, self.row_data[3])


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado.
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.modificar).place(x=0, y =100, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos.
        sql = """update beca set nom_bec = %(nom_bec)s
            where id_beca = %(id_beca)s"""
        self.proyecto.run_sql(sql, {"id_beca": int(self.row_data[0]),
            "nom_bec": self.entry_nom_bec.get()
            })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_beca()

    #def fill_combo(self):
        #sql = "select id_beca, nom_bec, beca_act, fec_bec from beca"
        #self.data = self.proyecto.run_select(sql)
        #return [i[1] for i in self.data], [i[0] for i in self.data]
