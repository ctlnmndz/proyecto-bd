import tkinter as tk
from tkinter import ttk

from beneficio import beneficio
from estudiante import estudiante
from tkinter import messagebox

class registro_beneficio:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Registro beneficio")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_registro_beneficio()
        self.config_buttons_registro_beneficio()

    def config_treeview_registro_beneficio(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "matricula")
        self.treeview.heading("#1", text = "beneficio")
        self.treeview.heading("#2", text = "fecha adquirido")
        self.treeview.heading("#3", text = "beneficio activo")
        self.treeview.column("#0", minwidth = 100, width = 160, stretch = False)
        self.treeview.column("#1", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#2", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#3", minwidth = 160, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_registro_beneficio()
        self.root.after(0, self.llenar_treeview_registro_beneficio)

    def config_buttons_registro_beneficio(self):
        tk.Button(self.root, text="Insertar registro beneficio",
            command = self.__insertar_registro_beneficio).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar registro beneficio",
            command = self.__modificar_registro_beneficio).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar registro beneficio",
            command = self.__eliminar_registro_beneficio).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_registro_beneficio(self):
        sql = """select matricula_estudiante, beneficio_id_bene, fec_bene, bene_act
        from estudiante_has_beneficio join beneficio on beneficio.id_bene = estudiante_has_beneficio.beneficio_id_bene
        join estudiante on estudiante.matricula = estudiante_has_beneficio.matricula_estudiante; """

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data


    def __insertar_registro_beneficio(self):
        insertar_registro_beneficio(self.proyecto, self)


    def __modificar_registro_beneficio(self):
        if(self.treeview.focus() != ""):
            sql = """select estudiante.matricula, beneficio.id_bene, fec_bene, bene_act from estudiante_has_beneficio join estudiante
            join beneficio on estudiante.matricula = estudiante_has_beneficio.matricula_estudiante and
            beneficio.id_bene = estudiante_has_beneficio.beneficio_id_bene
            where estudiante.matricula = %(estudiante.matricula)s"""
            row_data = self.proyecto.run_select_filter(sql, {"estudiante.matricula": self.treeview.focus()})[0]
            modificar_registro_beneficio(self.proyecto, self, row_data)

    def __eliminar_registro_beneficio(self):
            if(self.treeview.focus() != ""):
                if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                    sql = "delete from estudiante_has_beneficio where matricula_estudiante = %(matricula_estudiante)s"
                    self.proyecto.run_sql(sql, {"matricula_estudiante": self.treeview.focus()})
                    self.llenar_treeview_registro_beneficio()


class insertar_registro_beneficio:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('400x150')
        self.insert_datos.title("Insertar registro beneficio")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Estudiante: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "Beneficio: ").place(x = 10, y = 30, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "fecha adquerido: ").place(x = 10, y = 50, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "beneficio activo: (1:Si 2:No)").place(x = 10, y = 70, width = 150, height = 20)


    def config_entry(self):
        self.combo_estudiante = ttk.Combobox(self.insert_datos)
        self.combo_estudiante.place(x = 160, y = 10, width = 200, height= 20)
        self.combo_estudiante["values"], self.idse = self.fill_combo_estudiante()
        self.combo_beneficio = ttk.Combobox(self.insert_datos)
        self.combo_beneficio.place(x = 160, y = 30, width = 200, height= 20)
        self.combo_beneficio["values"], self.idsb = self.fill_combo_beneficio()

        self.entry_fec_bene = tk.Entry(self.insert_datos)
        self.entry_fec_bene.place(x = 160, y = 50, width = 200, height = 20)
        self.entry_bene_act = tk.Entry(self.insert_datos)
        self.entry_bene_act.place(x = 160, y = 70, width = 200, height = 20)


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =110, width = 300, height = 30)

    def fill_combo_estudiante(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_combo_beneficio(self):
        sql = "select id_bene, nom_bene from beneficio"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into estudiante_has_beneficio (matricula_estudiante, beneficio_id_bene, fec_bene, bene_act)
                values (%(matricula_estudiante)s,%(beneficio_id_bene)s, %(fec_bene)s, %(bene_act)s)"""
        self.proyecto.run_sql(sql, {
                    "matricula_estudiante": self.idse[self.combo_estudiante.current()],
                    "beneficio_id_bene": self.idsb[self.combo_beneficio.current()],
                    "fec_bene" : self.entry_fec_bene.get(),
                    "bene_act": self.entry_bene_act.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro_beneficio()

class modificar_registro_beneficio:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('400x150')
        self.insert_datos.title("Modificar registro beneficio")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Estudiante: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "Beneficio: ").place(x = 10, y = 30, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "fecha adquerido: ").place(x = 10, y = 50, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "bene activo: (1:Si 2:No)").place(x = 10, y = 70, width = 150, height = 20)

    def config_entry(self):
        self.combo_estudiante = ttk.Combobox(self.insert_datos)
        self.combo_estudiante.place(x = 160, y = 10, width = 200, height= 20)
        self.combo_estudiante["values"], self.idse = self.fill_combo_estudiante()
        self.combo_beneficio = ttk.Combobox(self.insert_datos)
        self.combo_beneficio.place(x = 160, y = 30, width = 200, height= 20)
        self.combo_beneficio["values"], self.idsb = self.fill_combo_beneficio()

        self.entry_fec_bene = tk.Entry(self.insert_datos)
        self.entry_fec_bene.place(x = 160, y = 50, width = 200, height = 20)
        self.entry_bene_act = tk.Entry(self.insert_datos)
        self.entry_bene_act.place(x = 160, y = 70, width = 200, height = 20)

        self.combo_estudiante.insert(0, self.row_data[0])
        self.combo_beneficio.insert(0, self.row_data[1])
        self.entry_fec_bene.insert(0, self.row_data[2])
        self.entry_bene_act.insert(0, self.row_data[3])


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =110, width = 300, height = 30)

    def fill_combo_estudiante(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def fill_combo_beneficio(self):
        sql = "select id_bene, nom_bene from beneficio"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]

    def __modificar(self): #Insercion en la base de datos.
        sql = """ update estudiante_has_beneficio set matricula_estudiante = %(matricula_estudiante)s,
                beneficio_id_bene = %(beneficio_id_bene)s, fec_bene = %(fec_bene)s, bene_act = %(bene_act)s
                where matricula_estudiante = %(matricula_estudiante)s"""
        self.proyecto.run_sql(sql, {
                    "matricula_estudiante": self.idse[self.combo_estudiante.current()],
                    "beneficio_id_bene": self.idsb[self.combo_beneficio.current()],
                    "fec_bene" : self.entry_fec_bene.get(),
                    "bene_act": self.entry_bene_act.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro_beneficio()
