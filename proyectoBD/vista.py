import tkinter as tk
from tkinter import ttk

class mostrar_vista:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        self.root = tk.Toplevel()
        self.root.geometry('600x400')
        self.root.title("Vista Estudiantes")
        self.root.resizable(width=0, height=0)

        self.root.transient(root)

        #
        self.config_treeview_vista()
        self.config_button_vista()

    def config_treeview_vista(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "Nombre")
        self.treeview.heading("#1", text = "Apellido")
        self.treeview.heading("#2", text = "Carrera")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 260, width = 260, stretch = False)
        self.treeview.column("#2", minwidth = 100, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_vista()
        self.root.after(0, self.llenar_treeview_vista)

    def llenar_treeview_vista(self):
        print("generando vista")
        sql = """ select * from Ver_Carrera_Estudiante"""

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2]), iid = i[0])
            self.data = data

    def __mostrar(self):
        mostrar(self.proyecto, self)


class mostrar:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_button()

    def config_button(self):
        tk.Button(self.insert_datos, text = "Generar",
            command = self.__insertar).place(x=0, y =150, width = 200, height = 20)

    def __insertar(self):
        sql = """ create or replace view Ver_Carrera_Estudiante as select nombre, apellido, carrera.nom_carrer
                  from estudiante join carrera on carrera.id_carrer = estudiante.carrera_id_carrer"""
        self.proyecto.run_sql(sql, self.treeview.get_children())
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro()
