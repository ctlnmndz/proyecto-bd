-- MySQL dump 10.13  Distrib 8.0.26, for Linux (x86_64)
--
-- Host: localhost    Database: Proyecto_DB
-- ------------------------------------------------------
-- Server version	8.0.26-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `beca`
--

DROP TABLE IF EXISTS `beca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beca` (
  `id_beca` int NOT NULL AUTO_INCREMENT,
  `nom_bec` varchar(45) NOT NULL,
  PRIMARY KEY (`id_beca`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beca`
--

LOCK TABLES `beca` WRITE;
/*!40000 ALTER TABLE `beca` DISABLE KEYS */;
INSERT INTO `beca` VALUES (1,'gratuidad');
/*!40000 ALTER TABLE `beca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `beneficio`
--

DROP TABLE IF EXISTS `beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `beneficio` (
  `id_bene` int NOT NULL AUTO_INCREMENT,
  `nom_bene` varchar(45) NOT NULL,
  PRIMARY KEY (`id_bene`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `beneficio`
--

LOCK TABLES `beneficio` WRITE;
/*!40000 ALTER TABLE `beneficio` DISABLE KEYS */;
INSERT INTO `beneficio` VALUES (1,'compuatdor'),(2,'atlas'),(6,'escritorio');
/*!40000 ALTER TABLE `beneficio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carrera`
--

DROP TABLE IF EXISTS `carrera`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carrera` (
  `id_carrer` varchar(10) NOT NULL,
  `nom_carrer` varchar(45) NOT NULL,
  `cant_vac` int NOT NULL,
  `num_sem` int NOT NULL,
  PRIMARY KEY (`id_carrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrera`
--

LOCK TABLES `carrera` WRITE;
/*!40000 ALTER TABLE `carrera` DISABLE KEYS */;
INSERT INTO `carrera` VALUES ('2020ASB','bioinfo',30,12),('dsadasd12','odonto',40,11);
/*!40000 ALTER TABLE `carrera` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `id_ciu` int NOT NULL AUTO_INCREMENT,
  `nom_ciu` varchar(45) NOT NULL,
  PRIMARY KEY (`id_ciu`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (4,'talca'),(9,'pelluhue'),(10,'iloca'),(12,'ciudadfea'),(13,'dasda'),(14,'as'),(17,'gauyaco'),(19,'san esteban'),(20,'san petrov');
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiante`
--

DROP TABLE IF EXISTS `estudiante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estudiante` (
  `matricula` varchar(10) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellido` varchar(100) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `rut` varchar(45) NOT NULL,
  `carrera_id_carrer` varchar(50) NOT NULL,
  `ciudad_id_ciu` int NOT NULL,
  `fec_ingreso` date NOT NULL,
  `correo` varchar(50) NOT NULL,
  PRIMARY KEY (`matricula`),
  KEY `fk_estudiante_ciudad1_idx` (`ciudad_id_ciu`),
  KEY `fk_estudiante_carrera1_idx` (`carrera_id_carrer`),
  CONSTRAINT `fk_estudiante_carrera1` FOREIGN KEY (`carrera_id_carrer`) REFERENCES `carrera` (`id_carrer`),
  CONSTRAINT `fk_estudiante_ciudad1` FOREIGN KEY (`ciudad_id_ciu`) REFERENCES `ciudad` (`id_ciu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiante`
--

LOCK TABLES `estudiante` WRITE;
/*!40000 ALTER TABLE `estudiante` DISABLE KEYS */;
INSERT INTO `estudiante` VALUES ('2018320012','catalina','morales','56987482522','21621427-9','2020ASB',17,'2018-06-24','quechoro@gmail.com'),('2018321014','felipe','villalba','56984788825','20565935-4','2020ASB',17,'2019-01-17','sdasdad@gmail.com'),('2019430004','jose','vera','56921263998','19412621-8','dsadasd12',4,'2019-01-23','algobacan@gmail.com');
/*!40000 ALTER TABLE `estudiante` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiante_has_beca`
--

DROP TABLE IF EXISTS `estudiante_has_beca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estudiante_has_beca` (
  `estudiante_matricula` varchar(10) NOT NULL,
  `beca_id_beca` int NOT NULL,
  `fec_bec` date NOT NULL,
  `beca_act` tinyint NOT NULL,
  PRIMARY KEY (`estudiante_matricula`,`beca_id_beca`),
  KEY `fk_estudiante_has_beca_beca1_idx` (`beca_id_beca`),
  KEY `fk_estudiante_has_beca_estudiante1_idx` (`estudiante_matricula`),
  CONSTRAINT `fk_estudiante_has_beca_beca1` FOREIGN KEY (`beca_id_beca`) REFERENCES `beca` (`id_beca`),
  CONSTRAINT `fk_estudiante_has_beca_estudiante1` FOREIGN KEY (`estudiante_matricula`) REFERENCES `estudiante` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiante_has_beca`
--

LOCK TABLES `estudiante_has_beca` WRITE;
/*!40000 ALTER TABLE `estudiante_has_beca` DISABLE KEYS */;
INSERT INTO `estudiante_has_beca` VALUES ('2018321014',1,'2020-09-30',1),('2019430004',1,'2020-01-23',1);
/*!40000 ALTER TABLE `estudiante_has_beca` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estudiante_has_beneficio`
--

DROP TABLE IF EXISTS `estudiante_has_beneficio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `estudiante_has_beneficio` (
  `matricula_estudiante` varchar(10) NOT NULL,
  `beneficio_id_bene` int NOT NULL,
  `fec_bene` date NOT NULL,
  `bene_act` tinyint NOT NULL,
  PRIMARY KEY (`matricula_estudiante`,`beneficio_id_bene`),
  KEY `fk_estudiante_has_beneficio_beneficio1_idx` (`beneficio_id_bene`),
  KEY `fk_estudiante_has_beneficio_estudiante1_idx` (`matricula_estudiante`),
  CONSTRAINT `fk_estudiante_has_beneficio_beneficio1` FOREIGN KEY (`beneficio_id_bene`) REFERENCES `beneficio` (`id_bene`),
  CONSTRAINT `fk_estudiante_has_beneficio_estudiante1` FOREIGN KEY (`matricula_estudiante`) REFERENCES `estudiante` (`matricula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estudiante_has_beneficio`
--

LOCK TABLES `estudiante_has_beneficio` WRITE;
/*!40000 ALTER TABLE `estudiante_has_beneficio` DISABLE KEYS */;
INSERT INTO `estudiante_has_beneficio` VALUES ('2018320012',2,'2001-04-10',1),('2018321014',1,'2020-06-30',1);
/*!40000 ALTER TABLE `estudiante_has_beneficio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recurso`
--

DROP TABLE IF EXISTS `recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recurso` (
  `id_rec` int NOT NULL AUTO_INCREMENT,
  `nom_rec` varchar(45) NOT NULL,
  `tipo_recurso_id_tipo` int NOT NULL,
  PRIMARY KEY (`id_rec`),
  KEY `fk_recurso_tipo_recurso1_idx` (`tipo_recurso_id_tipo`),
  CONSTRAINT `fk_recurso_tipo_recurso1` FOREIGN KEY (`tipo_recurso_id_tipo`) REFERENCES `tipo_recurso` (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recurso`
--

LOCK TABLES `recurso` WRITE;
/*!40000 ALTER TABLE `recurso` DISABLE KEYS */;
INSERT INTO `recurso` VALUES (1,'atlas',3),(3,'cuaderno',4),(4,'maqueta',5);
/*!40000 ALTER TABLE `recurso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regis_recurso`
--

DROP TABLE IF EXISTS `regis_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `regis_recurso` (
  `recurso_id_rec` int NOT NULL,
  `estudiante_matricula` varchar(10) NOT NULL,
  `fec_rec` date NOT NULL,
  `fec_lim` date DEFAULT NULL,
  PRIMARY KEY (`recurso_id_rec`,`estudiante_matricula`),
  KEY `fk_recurso_has_estudiante_estudiante1_idx` (`estudiante_matricula`),
  KEY `fk_recurso_has_estudiante_recurso1_idx` (`recurso_id_rec`),
  CONSTRAINT `fk_recurso_has_estudiante_estudiante1` FOREIGN KEY (`estudiante_matricula`) REFERENCES `estudiante` (`matricula`),
  CONSTRAINT `fk_recurso_has_estudiante_recurso1` FOREIGN KEY (`recurso_id_rec`) REFERENCES `recurso` (`id_rec`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regis_recurso`
--

LOCK TABLES `regis_recurso` WRITE;
/*!40000 ALTER TABLE `regis_recurso` DISABLE KEYS */;
INSERT INTO `regis_recurso` VALUES (1,'2018321014','2020-03-06','2020-06-06');
/*!40000 ALTER TABLE `regis_recurso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_recurso`
--

DROP TABLE IF EXISTS `tipo_recurso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_recurso` (
  `id_tipo` int NOT NULL AUTO_INCREMENT,
  `nom_tipo` varchar(45) NOT NULL,
  `cant_rec` int NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_recurso`
--

LOCK TABLES `tipo_recurso` WRITE;
/*!40000 ALTER TABLE `tipo_recurso` DISABLE KEYS */;
INSERT INTO `tipo_recurso` VALUES (3,'tecnología',50),(4,'libreria',50),(5,'prestamos',70);
/*!40000 ALTER TABLE `tipo_recurso` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-14  0:10:24
