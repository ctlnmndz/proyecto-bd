import tkinter as tk
from tkinter import ttk

from beca import beca
from estudiante import estudiante
from tkinter import messagebox

class registro_beca:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Registro beca")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_registro_beca()
        self.config_buttons_registro_beca()

    def config_treeview_registro_beca(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "matricula")
        self.treeview.heading("#1", text = "beca")
        self.treeview.heading("#2", text = "fecha adquerida")
        self.treeview.heading("#3", text = "beca activa")
        self.treeview.column("#0", minwidth = 100, width = 160, stretch = False)
        self.treeview.column("#1", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#2", minwidth = 160, width = 160, stretch = False)
        self.treeview.column("#3", minwidth = 160, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_registro_beca()
        self.root.after(0, self.llenar_treeview_registro_beca)

    def config_buttons_registro_beca(self):
        tk.Button(self.root, text="Insertar registro beca",
            command = self.__insertar_registro_beca).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar registro beca",
            command = self.__modificar_registro_beca).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar registro beca",
            command = self.__eliminar_registro_beca).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_registro_beca(self):
        sql = """select estudiante.matricula, beca.nom_bec, fec_bec, beca_act from estudiante_has_beca join estudiante 
            join beca on estudiante.matricula = estudiante_has_beca.estudiante_matricula and beca.id_beca = estudiante_has_beca.beca_id_beca """

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data  


    def __insertar_registro_beca(self):
        insertar_registro_beca(self.proyecto, self)


    def __modificar_registro_beca(self):
        if(self.treeview.focus() != ""):
            sql = """select estudiante.matricula, beca.nom_bec, fec_bec, beca_act from estudiante_has_beca join estudiante 
            join beca on estudiante.matricula = estudiante_has_beca.estudiante_matricula and beca.id_beca = estudiante_has_beca.beca_id_beca
            where estudiante.matricula = %(estudiante.matricula)s"""
            row_data = self.proyecto.run_select_filter(sql, {"estudiante.matricula": self.treeview.focus()})[0]
            modificar_registro_beca(self.proyecto, self, row_data)

    def __eliminar_registro_beca(self):
            if(self.treeview.focus() != ""):
                if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                    sql = "delete from estudiante_has_beca where estudiante_matricula = %(estudiante_matricula)s"
                    self.proyecto.run_sql(sql, {"estudiante_matricula": self.treeview.focus()})
                    self.llenar_treeview_registro_beca()


class insertar_registro_beca:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('400x150')
        self.insert_datos.title("Insertar registro beca")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Estudiante: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "Beca: ").place(x = 10, y = 30, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "fecha adquerida: ").place(x = 10, y = 50, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "beca activa: (1:Si 2:No)").place(x = 10, y = 70, width = 150, height = 20)


    def config_entry(self):
        self.combo_estudiante = ttk.Combobox(self.insert_datos)
        self.combo_estudiante.place(x = 160, y = 10, width = 200, height= 20)
        self.combo_estudiante["values"], self.idse = self.fill_combo_estudiante()
        self.combo_beca = ttk.Combobox(self.insert_datos)
        self.combo_beca.place(x = 160, y = 30, width = 200, height= 20)
        self.combo_beca["values"], self.idsb = self.fill_combo_beca()
       
        self.entry_fec_bec = tk.Entry(self.insert_datos)
        self.entry_fec_bec.place(x = 160, y = 50, width = 200, height = 20)
        self.entry_beca_act = tk.Entry(self.insert_datos)
        self.entry_beca_act.place(x = 160, y = 70, width = 200, height = 20)
        
       
    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =110, width = 300, height = 30)

    def fill_combo_estudiante(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
   
    def fill_combo_beca(self):
        sql = "select id_beca, nom_bec from beca"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into estudiante_has_beca (estudiante_matricula, beca_id_beca, fec_bec, beca_act)
                values (%(estudiante_matricula)s,%(beca_id_beca)s, %(fec_bec)s, %(beca_act)s)"""
        self.proyecto.run_sql(sql, {
                    "estudiante_matricula": self.idse[self.combo_estudiante.current()],
                    "beca_id_beca": self.idsb[self.combo_beca.current()],
                    "fec_bec" : self.entry_fec_bec.get(),
                    "beca_act": self.entry_beca_act.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro_beca()

class modificar_registro_beca:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('400x150')
        self.insert_datos.title("Modificar registro beca")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Estudiante: ").place(x = 10, y = 10, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "Beca: ").place(x = 10, y = 30, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "fecha adquerida: ").place(x = 10, y = 50, width = 150, height = 20)
        tk.Label(self.insert_datos, text = "beca activa: (1:Si 2:No)").place(x = 10, y = 70, width = 150, height = 20)

    def config_entry(self):
        self.combo_estudiante = ttk.Combobox(self.insert_datos)
        self.combo_estudiante.place(x = 160, y = 10, width = 200, height= 20)
        self.combo_estudiante["values"], self.idse = self.fill_combo_estudiante()
        self.combo_beca = ttk.Combobox(self.insert_datos)
        self.combo_beca.place(x = 160, y = 30, width = 200, height= 20)
        self.combo_beca["values"], self.idsb = self.fill_combo_beca()
       
        self.entry_fec_bec = tk.Entry(self.insert_datos)
        self.entry_fec_bec.place(x = 160, y = 50, width = 200, height = 20)
        self.entry_beca_act = tk.Entry(self.insert_datos)
        self.entry_beca_act.place(x = 160, y = 70, width = 200, height = 20)
        
        self.combo_estudiante.insert(0, self.row_data[0])
        self.combo_beca.insert(0, self.row_data[1])
        self.entry_fec_bec.insert(0, self.row_data[2])
        self.entry_beca_act.insert(0, self.row_data[3])


    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =110, width = 300, height = 30)

    def fill_combo_estudiante(self):
        sql = "select matricula, nombre from estudiante"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
   
    def fill_combo_beca(self):
        sql = "select id_beca, nom_bec from beca"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
        
    def __modificar(self): #Insercion en la base de datos.
        sql = """ update estudiante_has_beca set estudiante_matricula = %(estudiante_matricula)s,
                beca_id_beca = %(beca_id_beca)s, fec_bec = %(fec_bec)s, beca_act = %(beca_act)s 
                where estudiante_matricula = %(estudiante_matricula)s"""
        self.proyecto.run_sql(sql, {
                    "estudiante_matricula": self.idse[self.combo_estudiante.current()],
                    "beca_id_beca": self.idsb[self.combo_beca.current()],
                    "fec_bec" : self.entry_fec_bec.get(),
                    "beca_act": self.entry_beca_act.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_registro_beca()
