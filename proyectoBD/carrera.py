import tkinter as tk
from tkinter import ttk
from tkinter import messagebox


class carrera:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []
        
        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel() 
        self.root.geometry('705x400')
        self.root.title("Carreras")
        self.root.resizable(width=10, height=10)
        
        # toplevel modal
        self.root.transient(root)
        
        #
        self.__config_treeview_carrera()
        self.__config_buttons_carrera()

    def __config_treeview_carrera(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2", "#3"))
        self.treeview.heading("#0", text = "id_carrer")
        self.treeview.heading("#1", text = "nom_carrer")
        self.treeview.heading("#2", text = "cant_vac")
        self.treeview.heading("#3", text = "num_sem")
        self.treeview.column("#0", minwidth = 100, width = 100, stretch = False)
        self.treeview.column("#1", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#2", minwidth = 200, width = 200, stretch = False)
        self.treeview.column("#3", minwidth = 200, width = 200, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 700)
        self.llenar_treeview_carrera()
        self.root.after(0, self.llenar_treeview_carrera)

    def __config_buttons_carrera(self):
        tk.Button(self.root, text="Insertar carrera", 
            command = self.__insertar_carrera).place(x = 0, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Modificar carrera", 
            command = self.__modificar_carrera).place(x = 200, y = 350, width = 200, height = 50)
        tk.Button(self.root, text="Eliminar carrera", 
            command = self.__eliminar_carrera).place(x = 400, y = 350, width = 200, height = 50)
       

    def llenar_treeview_carrera(self):
        sql = "select *from carrera;"
        data = self.proyecto.run_select(sql)
        
        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())#Elimina todos los rows del treeview
            for i in data:
                self.treeview.insert("", "end", text = i[0], 
                    values = (i[1], i[2], i[3]), iid = i[0])
            self.data = data

    def __insertar_carrera(self):
        insertar_carrera(self.proyecto, self)

    def __modificar_carrera(self):
        if(self.treeview.focus() != ""):
            sql = "select * from carrera where id_carrer = %(id_carrer)s"
            row_data = self.proyecto.run_select_filter(sql, {"id_carrer": self.treeview.focus()})[0]
            modificar_carrera(self.proyecto, self, row_data)
            

    def __eliminar_carrera(self):
        if(self.treeview.focus() != ""):
            if messagebox.askyesno(message="¿Está seguro que quiere eliminarlo?", title = "Precaución") == True:
                sql = "delete from carrera where id_carrer = %(id_carrer)s"
                self.proyecto.run_sql(sql, {"id_carrer": self.treeview.focus()})
                self.llenar_treeview_carrera()


class insertar_carrera:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.__config_window()
        self.__config_label()
        self.__config_entry()
        self.__config_button()

    def __config_window(self):
        self.insert_datos.geometry('400x200')
        self.insert_datos.title("Insertar carrera")
        self.insert_datos.resizable(width=0, height=0)

    def __config_label(self):
        tk.Label(self.insert_datos, text = "ID carrera: ").place(x = 10, y = 10, width = 110, height = 20)
        tk.Label(self.insert_datos, text = "Nombre carrera: ").place(x = 10, y = 40, width = 110, height = 20)
        tk.Label(self.insert_datos, text = "Cantidad vacante: ").place(x = 10, y = 70, width = 120, height = 20)
        tk.Label(self.insert_datos, text = "Cantidad semestre: ").place(x = 10, y = 100, width = 130, height = 20)

    def __config_entry(self):
        self.entry_id_carrer = tk.Entry(self.insert_datos)
        self.entry_id_carrer.place(x = 140, y = 10, width = 250, height = 20)
        self.entry_nom_carrer = tk.Entry(self.insert_datos)
        self.entry_nom_carrer.place(x = 140, y = 40, width = 250, height = 20)
        self.entry_cant_vac = tk.Entry(self.insert_datos)
        self.entry_cant_vac.place(x = 140, y = 70, width = 250, height = 20)
        self.entry_num_sem = tk.Entry(self.insert_datos)
        self.entry_num_sem.place(x = 140, y = 100, width = 250, height = 20)


    def __config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.__insertar).place(x=85, y =170, width = 200, height = 20)



    def __insertar(self): #Insercion en la base de datos.
        sql = """insert carrera (id_carrer, nom_carrer, cant_vac, num_sem) 
            values (%(id_carrer)s, %(nom_carrer)s, %(cant_vac)s, %(num_sem)s)"""
        self.proyecto.run_sql(sql, {"id_carrer": self.entry_id_carrer.get(), 
            "nom_carrer": self.entry_nom_carrer.get(), 
            "cant_vac": self.entry_cant_vac.get(),
            "num_sem": self.entry_num_sem.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_carrera()

class modificar_carrera:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('400x200')
        self.insert_datos.title("Modificar carrera")
        self.insert_datos.resizable(width=0, height=0)
    
    def config_label(self):
        #
        tk.Label(self.insert_datos, text = "ID carrera: ").place(x = 10, y = 10, width = 110, height = 20)
        tk.Label(self.insert_datos, text = "Nombre carrera: ").place(x = 10, y = 40, width = 110, height = 20)
        tk.Label(self.insert_datos, text = "Cantidad vacante: ").place(x = 10, y = 70, width = 120, height = 20)
        tk.Label(self.insert_datos, text = "Cantidad semestre: ").place(x = 10, y = 100, width = 130, height = 20)

    def config_entry(self): #se configuran los input
        self.entry_id_carrer = tk.Entry(self.insert_datos)
        self.entry_id_carrer.place(x = 140, y = 10, width = 250, height = 20)
        self.entry_nom_carrer = tk.Entry(self.insert_datos)
        self.entry_nom_carrer.place(x = 140, y = 40, width = 250, height = 20)
        self.entry_cant_vac = tk.Entry(self.insert_datos)
        self.entry_cant_vac.place(x = 140, y = 70, width = 250, height = 20)
        self.entry_num_sem = tk.Entry(self.insert_datos)
        self.entry_num_sem.place(x = 140, y = 100, width = 250, height = 20)
        self.entry_id_carrer.insert(0, self.row_data[0])
        self.entry_nom_carrer.insert(0, self.row_data[1])
        self.entry_cant_vac.insert(0, self.row_data[2])
        self.entry_num_sem.insert(0, self.row_data[3])


    def config_button(self): #Botón aceptar, llama a la función modificar cuando es clickeado. 
        tk.Button(self.insert_datos, text = "Aceptar", 
            command = self.modificar).place(x=85, y =170, width = 200, height = 20)

    def modificar(self): #Insercion en la base de datos. 
        sql = """update carrera set nom_carrer = %(nom_carrer)s, cant_vac = %(cant_vac)s,
            num_sem = %(num_sem)s where id_carrer = %(id_carrer)s"""
        self.proyecto.run_sql(sql, {"id_carrer": str(self.row_data[0]),
            "id_carrer": self.entry_id_carrer.get(), 
            "nom_carrer": self.entry_nom_carrer.get(), 
            "cant_vac": self.entry_cant_vac.get(),
            "num_sem": self.entry_num_sem.get()})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_carrera()
