import tkinter as tk
from tkinter import ttk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import matplotlib.pyplot as plot


class scatter:
    def __init__(self, root, db):
        self.root = root
        self.proyecto = db
        self.graph = tk.Toplevel()
        self.graph.geometry('700x500')
        self.graph.title("Scatter")


        self.__config_grafica()

    def __config_grafica(self):

        fig, ax = plot.subplots()
        x, y = self.obtencion_datos()

        # Titulo
        plot.title("Scatter: Cantidad de recursos")

        plot.xticks(rotation=20)
        plot.xlabel("nombre recurso")
        plot.ylabel("Cantidad")
        plot.scatter(x, y, alpha=0.8, color = "purple")
        canvas = tk.Canvas(self.graph, width=700, height=500, background="white")
        canvas = FigureCanvasTkAgg(fig, master = self.graph)
        canvas.draw()
        canvas.get_tk_widget().pack()


    def obtencion_datos(self):
        sql = """select nom_tipo, cant_rec from tipo_recurso;"""

        data = self.proyecto.run_select(sql)
        # el eje X
        x = [i[0] for i in data]
        # el eje Y
        y = [i[1] for i in data]
        return x, y
