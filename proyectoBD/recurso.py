import tkinter as tk
from tkinter import ttk

from tipo_recurso import tipo_recurso
from tkinter import messagebox

class recurso:
    def __init__(self, root, db):
        self.proyecto = db
        self.data = []

        #Toplevel es una ventana que está un nivel arriba que la principal
        self.root = tk.Toplevel()
        self.root.geometry('750x400')
        self.root.title("Recursos")
        self.root.resizable(width=0, height=0)

        # toplevel modal
        self.root.transient(root)

        #
        self.config_treeview_recurso()
        self.config_buttons_recurso()

    def config_treeview_recurso(self):
        self.treeview = ttk.Treeview(self.root)
        self.treeview.configure(columns = ("#1", "#2"))
        self.treeview.heading("#0", text = "Id")
        self.treeview.heading("#1", text = "nombre recurso")
        self.treeview.heading("#2", text = "tipo recurso")
        self.treeview.column("#0", minwidth = 100, width = 120, stretch = False)
        self.treeview.column("#1", minwidth = 260, width = 260, stretch = False)
        self.treeview.column("#2", minwidth = 160, width = 160, stretch = False)
        self.treeview.place(x = 0, y = 0, height = 350, width = 750)
        self.llenar_treeview_recurso()
        self.root.after(0, self.llenar_treeview_recurso)

    def config_buttons_recurso(self):
        tk.Button(self.root, text="Insertar recurso",
            command = self.__insertar_recurso).place(x = 0, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Modificar recurso",
            command = self.__modificar_recurso).place(x = 250, y = 350, width = 250, height = 50)
        tk.Button(self.root, text="Eliminar recurso",
            command = self.__eliminar_recurso).place(x = 500, y = 350, width = 250, height = 50)

    def llenar_treeview_recurso(self):
        sql = """select id_rec, nom_rec, tipo_recurso.nom_tipo
        from recurso join tipo_recurso on tipo_recurso.id_tipo = recurso.tipo_recurso_id_tipo;"""

        data = self.proyecto.run_select(sql)

        if(data != self.data):
            self.treeview.delete(*self.treeview.get_children())
            for i in data:
                self.treeview.insert("", "end", text = i[0],
                    values = (i[1], i[2]), iid = i[0])
            self.data = data  


    def __insertar_recurso(self):
        insertar_recurso(self.proyecto, self)


    def __modificar_recurso(self):
        if(self.treeview.focus() != ""):
            sql = """select id_rec, nom_rec, tipo_recurso.nom_tipo
                from recurso join tipo_recurso on tipo_recurso.id_tipo = recurso.tipo_recurso_id_tipo
                where id_rec = %(id_rec)s"""
            row_data = self.proyecto.run_select_filter(sql, {"id_rec": self.treeview.focus()})[0]
            modificar_recurso(self.proyecto, self, row_data)

    def __eliminar_recurso(self):
            if(self.treeview.focus() != ""):
                if messagebox.askyesno(message="¿Realmente quieres eliminar este registro?", title = "Alerta") == True:
                    sql = "delete from recurso where id_rec = %(id_rec)s"
                    self.proyecto.run_sql(sql, {"id_rec": self.treeview.focus()})
                    self.llenar_treeview_recurso()


class insertar_recurso:
    def __init__(self, db, padre):
        self.padre = padre
        self.proyecto = db
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self):
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Insertar recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 100, height = 30)
        tk.Label(self.insert_datos, text = "Tipo recurso: ").place(x = 10, y = 50, width = 100, height = 30)


    def config_entry(self):
        self.entry_nom_rec = tk.Entry(self.insert_datos)
        self.entry_nom_rec.place(x = 110, y = 10, width = 180, height = 30)
        self.combo_tipo = ttk.Combobox(self.insert_datos)
        self.combo_tipo.place(x = 110, y = 50, width = 180, height= 30)
        self.combo_tipo["values"], self.ids = self.fill_combo_tipo()
       

    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__insertar).place(x=0, y =150, width = 300, height = 30)

    def fill_combo_tipo(self):
        sql = "select id_tipo, nom_tipo from tipo_recurso"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]


    def __insertar(self): #Insercion en la base de datos.
        sql = """insert into recurso (nom_rec, tipo_recurso_id_tipo)
                values (%(nom_rec)s,%(tipo_recurso_id_tipo)s)"""
        self.proyecto.run_sql(sql, {
                    "nom_rec": self.entry_nom_rec.get(),
                    "tipo_recurso_id_tipo": self.ids[self.combo_tipo.current()]})
        self.insert_datos.destroy()
        self.padre.llenar_treeview_recurso()

class modificar_recurso:
    def __init__(self, db, padre, row_data):
        self.padre = padre
        self.proyecto = db
        self.row_data = row_data
        self.insert_datos = tk.Toplevel()
        self.config_window()
        self.config_label()
        self.config_entry()
        self.config_button()

    def config_window(self): #Settings
        self.insert_datos.geometry('300x200')
        self.insert_datos.title("Modificar recurso")
        self.insert_datos.resizable(width=0, height=0)

    def config_label(self):
        tk.Label(self.insert_datos, text = "Nombre: ").place(x = 10, y = 10, width = 100, height = 30)
        tk.Label(self.insert_datos, text = "Tipo recurso: ").place(x = 10, y = 50, width = 100, height = 30)

    def config_entry(self):
        self.entry_nom_rec = tk.Entry(self.insert_datos)
        self.entry_nom_rec.place(x = 110, y = 10, width = 180, height = 30)
        self.combo_tipo = ttk.Combobox(self.insert_datos)
        self.combo_tipo.place(x = 110, y = 50, width = 180, height= 30)
        self.combo_tipo["values"], self.ids = self.fill_combo_tipo()
        
        self.entry_nom_rec.insert(0, self.row_data[1])
        self.combo_tipo.insert(0, self.row_data[2])



    def config_button(self):
        tk.Button(self.insert_datos, text = "Aceptar",
            command = self.__modificar).place(x=0, y =150, width = 300, height = 30)

    def fill_combo_tipo(self):
        sql = "select id_tipo, nom_tipo from tipo_recurso"
        self.data = self.proyecto.run_select(sql)
        return [i[1] for i in self.data], [i[0] for i in self.data]
        
        
    def __modificar(self): #Insercion en la base de datos.
        sql = """ update recurso set nom_rec = %(nom_rec)s,
                tipo_recurso_id_tipo = %(tipo_recurso_id_tipo)s
                where id_rec = %(id_rec)s"""
        self.proyecto.run_sql(sql, {"id_rec": int(self.row_data[0]),
                        "nom_rec": self.entry_nom_rec.get(),
                        "tipo_recurso_id_tipo": self.ids[self.combo_tipo.current()],
                        })
        self.insert_datos.destroy()
        self.padre.llenar_treeview_recurso()
